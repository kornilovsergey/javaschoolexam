package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (y == null) throw new IllegalArgumentException();

        List<Integer> position = new ArrayList<>();

        try {
            for (Object str :
                    x) {
                if (y.contains(str)) {
                    position.add(y.indexOf(str));
                } else
                    return false;
            }
        } catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }

        List temp = new ArrayList(position);

        Collections.sort(temp);

        // TODO: Implement the logic here
        return position.equals(temp);
    }
}
