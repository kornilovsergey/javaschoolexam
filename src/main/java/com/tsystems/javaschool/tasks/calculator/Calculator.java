package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
         * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (statement == null) return null;
        if (statement.equals("")) return null;

        char[] statementCharArray = statement.toCharArray();

        if (!convertToRPN(statementCharArray)) return null;
        else return countRPN(output.toCharArray());

        // TODO: Implement the logic here
   //     return "";
    }

    private final static char[] operators = {'-','+','/','*'};
    private static String output = "";
    static private boolean isSpace (char inputChar)
    {
        return inputChar == ' ';
    }

    static private boolean isOperator(char inputChar)
    {
        for (char checkChar :
                operators) {
            if(inputChar == checkChar)
                return true;
        }
        return false;
    }

    static private int checkPriority(char operation) {
        if (operation == '(') return 0;
        else if (operation == ')') return 1;
        else if (operation == '+' || operation == '-')
            return 2;
        else
            return 3;

    }

    static private boolean convertToRPN(char[] inputEvaluateStatement){
        long openedBracket = 0;
        Stack<Character> operationStack = new Stack<>();

        for (int i = 0; i < inputEvaluateStatement.length - 1; i++){
            if (isOperator(inputEvaluateStatement[i]) && isOperator(inputEvaluateStatement[i + 1]))
                return false;

            if (inputEvaluateStatement[i] == '.' && inputEvaluateStatement[i + 1] == '.')
                return false;

            if (inputEvaluateStatement[i] == ',')
                return false;
        }



        for(int i = 0; i < inputEvaluateStatement.length; i++){
            if (isSpace(inputEvaluateStatement[i])){
                continue;
            }

            if (Character.isDigit(inputEvaluateStatement[i])){
                while((Character.isDigit(inputEvaluateStatement[i])) || inputEvaluateStatement[i] == '.'){
                    output += inputEvaluateStatement[i];
                    i = i + 1;
                    if (i == (inputEvaluateStatement.length)) break;
                }
                output += " ";
                i--;
            }

            if (isOperator(inputEvaluateStatement[i])
                    || inputEvaluateStatement[i] == '('
                    || inputEvaluateStatement[i] == ')'){
                if (inputEvaluateStatement[i] == '(') {
                    openedBracket += 1;

                    long closedBracket = 0;
                    boolean check = false;

                    for (int d = i; d < inputEvaluateStatement.length; d ++){
                        if (inputEvaluateStatement[d] == ')') closedBracket += 1;
                    }
                    if (openedBracket == closedBracket) operationStack.push(inputEvaluateStatement[i]);
                    else {
                        return false;
                    }
                }
                else if (inputEvaluateStatement[i] == ')')
                {
                    char c = operationStack.pop();
                    while (c != '('){
                        output += Character.toString(c) + " ";
                        try {
                            c = operationStack.pop();
                        } catch (Exception exc){
                        }
                    }
                }
                else
                {
                    if (!operationStack.empty())
                        if (checkPriority(inputEvaluateStatement[i]) <= checkPriority(operationStack.peek()))
                            output += operationStack.pop().toString() + " ";
                    operationStack.push(inputEvaluateStatement[i]);
                }
            }
        }

        while (!operationStack.empty())
            output += operationStack.pop() + " ";
        return true;
    }

    static private String countRPN (char[] inputRPN ){
        double result = 0;
        Stack<Double> stack = new Stack<>();

        for (int i = 0; i < inputRPN.length; i ++){
            if (Character.isDigit(inputRPN[i])){
                String str = "";

                while((Character.isDigit(inputRPN[i])) || inputRPN[i] == '.'){
                    str += inputRPN[i];
                    i++;
                    if (i == inputRPN.length) break;
                }
                stack.push(Double.parseDouble(str));
                i--;
            } else if (isOperator(inputRPN[i]))
            {
                double a = stack.pop();
                double b = stack.pop();

                switch (inputRPN[i])
                {
                    case '/': {if (a == 0)
                        return null;
                        result = b / a;}
                    break;
                    case '*': result = b * a;
                        break;
                    case '+': result = b + a;
                        break;
                    case '-': result = b - a;
                }
                stack.push(result);
            }
        }

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        String roundedResult = decimalFormat.format(stack.peek());

        return roundedResult.replace(",", ".");



    }
}
