package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        try {
            Collections.sort(inputNumbers);
        }catch (Throwable e){
            throw new CannotBuildPyramidException();
        }
        int inputNumbersSize = inputNumbers.size();

        int countRow = 0;

        for (int i = 1; i < inputNumbers.size(); i++) {

            inputNumbersSize = inputNumbersSize - i;
            countRow++;

            if (inputNumbersSize < 0) {
                throw new CannotBuildPyramidException();
            } else if (inputNumbersSize == 0) break;
        }

        Deque<Integer> queueNum = new ArrayDeque<>(inputNumbers);

        int countCol = countRow * 2 - 1;

        int[][] mas = new int[countRow][countCol];
        int colOffset;

        for (int m = 0; m < countRow ; m ++) {
            colOffset = 1;
            for (int i = 0; i < countRow - m ; i++) {
                for (int d = 0; d < countCol; d++) {
                    if (m % 2 == 0) {
                        if (d == (countCol - colOffset) / 2) {
                            mas[i + m][d] = 1;
                        }
                        if (d == (countCol + colOffset - 1) / 2 && i != 0) {
                            mas[i + m][d] = 1;
                        }
                    }
                }
                colOffset += 2;
            }
        }

        for (int i = 0; i < countRow; i ++){
            for (int d = 0; d < countCol; d ++){
                if (mas[i][d] == 1) mas[i][d] = queueNum.pop();
            }
        }

        // TODO : Implement your solution here
        return mas;
    }

}
